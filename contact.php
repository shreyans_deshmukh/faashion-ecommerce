<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-quiv="X-UA-Compatibe" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css" integrity="sha384-Bfad6CLCknfcloXFOyFnlgtENryhrpZCe29RTifKEixXQZ38WheV+i/6YWSzkz3V" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Fashion Ecommerce</title>
</head>


<body data-spy="scroll" data-target="#main-nav" >
<?php require('include/config.php');?>

<?php

// if($conn){
//   echo "Database connected";
// }

/*

client -> Server

https

http methods

1) GET method -> Read data from database and GET URL parameters

2)POST method -> Read data from the form tags using "name" attribute

3)PUT method -> Update data in database

4)DELETE method -> hard delete data from databse

*/

// if(isset($POST['Submit'])){

//   $fname = $POST['name'];
//   $lname = $POST['name'];
//   $mail = $POST['email'];
//   $contact = $POST['contact'];
//   $message = $POST['message'];

//   $sql = "INSERT INTO `cloth` (`first_name`, `last_name`, `email`, `contact`, `message`) VALUES ('$fname', '$lname', '$mail', $contact, '$message')";
//   $query_run = mysqli_query($conn, $sql);

//   if($query_run){
//     echo "Data added";
//   }

//   // echo "data received " . $fname . " " . $lname." ". $contact ." ". $mail . " " . $message;
// }

if(isset($_POST['Submit'])) {
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $email = $_POST['email'];
    $mobile = $_POST['contact'];
    $message = $_POST['message'];


    // Insert user data into table
    $result = mysqli_query($conn, "INSERT INTO cloth(first_name, last_name, email, contact, message) VALUES ('$fname', '$lname', '$email', '$mobile', '$message')");

    // Show message when user added
    header("Location: contact.php");
}
?>  
  <!--logo-bar-->
  <!--navbar-->
  <?php require('include/navbar.php');?>
  
  

  <!--Page Header-->
  <header id="page-header">
      <div class="container">
          <div class="row">
              <div class="col-md-6 m-auto text-center">
                  <h2>Contact Us</h2>
                  <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab, laborum!</p>
              </div>
          </div>
      </div>
  </header>

  <!--Contact-->
  <section id="contact" class="py-3">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card p-4">
                        <div class="card-body">
                            <h4>Get in Touch</h4>
                            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et, amet?</p>
                            <h4>Address</h4>
                            <p>Lorem ipsum dolor sit amet.</p>
                            <h4>Email</h4>
                            <p>Info@Info.com</p>
                            <h4>Phone No</h4>
                            <p>(+91)9999999999</p>
                        </div>
                    </div>
                </div>
               
                <div class="col-md-8">
                    <div class="card p-4">
                        <div class="card-body">
                            <h3 class="text-center">PLease fill out  this form to contact us</h3>
                            <hr>
                            <form action="contact.php" id="contactForm" method="post" name="form1">
		                          <table width="25%" border="0">
			                          <tr>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="fname" type="text" class="form-control" placeholder=" First Name">
                                    </div>
                                </div>
			                          </tr>
                                <tr>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="lname" type="text" class="form-control" placeholder="Last Name">
                                    </div>
                                </div>
			                          </tr>
			                          <tr>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="email" type="email" class="form-control" placeholder="Email">
                                    </div>
                                </div>
			                          </tr>
			                          <tr>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="contact" type="contact" class="form-control" placeholder="Phone">
                                    </div>
                                </div>
			                          </tr>
                                <tr>
                                  <div class="col-md-6">
                                     <div class="form-group">
                                        <input name="message" class="form-control" placeholder="Message">
                                    </div>
                                  </div>
			                          </tr>
			                          <tr>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="Submit" type="Submit"  value="Submit" class="btn btn-outline-danger btn-block">
                                    </div>
                                </div>
			                          </tr>
		                          </table>
                           </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
   </section>

   <!--Team Members-->
   <section id="Member" class="py-5 text-center bg-dark text-white">
       <div class="container">
           <h1>Our Team Members</h1>
           <hr>
           <div class="row">
               <div class="col-md-3">
                   <img src="img/person1.jpg" alt="" class="img-fluid rounded-circle mb-2">
                   <h4>Harry Potter</h4>
                   <small>CEO</small>
               </div>
               <div class="col-md-3">
                  <img src="img/person2.jpg" alt="" class="img-fluid rounded-circle mb-2">
                  <h4>Hermione Granger</h4>
                  <small>Marketing Manager</small>
                </div>
                <div class="col-md-3">
                    <img src="img/person3.jpg" alt="" class="img-fluid rounded-circle mb-2">
                    <h4>Ron Weasely</h4>
                    <small>Business Manger</small>
                </div>
                <div class="col-md-3">
                    <img src="img/person4.jpg" alt="" class="img-fluid rounded-circle mb-2">
                    <h4>Lord Voldemort</h4>
                    <small>Web Developer</small>
                </div>
           </div>
       </div>
   </section>

  <!--Footer-->
  <footer id="main-footer" class="text-center p-4">
    <div class="container">
      <div class="row">
        <div class="col">
          <p>Copyright &copy;<span id="year">Fashion Ecommerce</span></p>
        </div>
      </div>
    </div>
 </footer>







  

  <script src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous"></script>   
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" 
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" 
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" 
    integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" 
    crossorigin="anonymous"></script>
  
    

   <script>
     //Get the current year for the copyright
     $('#year').text(new Date().getFullYear());

     $(document).ready(function () {
        clearForm();
      });

     function clearForm(){
       document.getElementById('contactForm'.reset();)
     }

   </script> 
</body>
</html>
