<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-quiv="X-UA-Compatibe" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css" integrity="sha384-Bfad6CLCknfcloXFOyFnlgtENryhrpZCe29RTifKEixXQZ38WheV+i/6YWSzkz3V" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" integrity="sha256-HAaDW5o2+LelybUhfuk0Zh2Vdk8Y2W2UeKmbaXhalfA=" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css">
    <title>Fashion Ecommerce</title>
</head>


<body data-spy="scroll" data-target="#main-nav" >
  
  
  <!--logo-bar-->
  <!--navbar-->
<?php require('include/navbar.php');?>
  

<!--showcase slider-->
<section id="showcase">
  
    <div id="mycarousel" class="carousel slide " data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#mycarousel" data-slide-to="0" class="active"></li>
        <li data-target="#mycarousel" data-slide-to="1" class="active"></li>
        <li data-target="#mycarousel" data-slide-to="2" class="active"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item carousel-image-1 active">
          <div class="container">
            <div class="carousel-caption d-none d-sm-block text-right mb-5">
              <p class="lead">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Animi, expedita.</p>
              <a href="#" class="btn btn-danger">lorem</a>       
            </div>
          </div>
        </div>
        <div class="carousel-item carousel-image-2 ">
          <div class="container">
            <div class="carousel-caption d-none d-sm-block  mb-5">
              <p class="lead">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Animi, expedita.</p>    
            </div>
          </div>
        </div>
        <div class="carousel-item carousel-image-3 ">
          <div class="container">
            <div class="carousel-caption d-none d-sm-block text-right mb-5">
              <p class="lead">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Animi, expedita.</p>     
            </div>
          </div>
        </div>
      </div>
      <a href="#mycarousel" data-slide="prev" class="carousel-control-prev">
        <span class="carousel-control-prev-icon"></span>
      </a>
      <a href="#mycarousel" data-slide="next" class="carousel-control-next">
        <span class="carousel-control-next-icon"></span>
      </a>
    </div>
    <marquee  class=" bg-success" font-size="d-sm-block"><h5>Lorem ipsum dolor sit amet.</h5></marquee>
  </section>
  <!--Top Brand-->
  <div id="boxes">
    <div id="leftbox">
      <img class="card-img ml-auto"  src="img\fashion-men-s-individuality-black-and-white-157675.jpeg" style="height: 35rem; width: 25rem;" alt="card Image Top">
      
    </div>
    <div id="middlebox">
      <section id="gallery" class=" py-3">
        <div class="container " >
          <h1 class="text-center">TOP BRANDS</h1>
          <p class="text-center">Lorem, ipsum dolor.</p>
          <div class="row mb-4 ">
           <div class="col-mb-4 mx-auto pt-5 ml-5">
              <a href="https://source.unsplash.com/random/244x244" data-toggle="lightbox" data-gallery="img-gallery" style="height: 35rem; width: 25rem;">
              <img src="https://source.unsplash.com/random/244x244" alt="" class="img-fluid align-middle">
              </a>
            </div>
    
            <div class="col-mb-4 mx-auto pt-5 ml-5">
              <a href="https://source.unsplash.com/random/245x245" data-toggle="lightbox" data-gallery="img-gallery" style="height: 35rem; width: 25rem;">
              <img src="https://source.unsplash.com/random/245x245" alt="" class="img-fluid">
              </a>
            </div>
    
            <div class="col-mb-4 mx-auto pt-5 ml-5">
              <a href="https://source.unsplash.com/random/246x246" data-toggle="lightbox" data-gallery="img-gallery" style="height: 35rem; width: 25rem;" >
              <img src="https://source.unsplash.com/random/246x246" alt="" class="img-fluid">
              </a>
            </div>
         </div>
    
          <div class="row mb-4">
            <div class="col-mb-4 mx-auto pt-5 ml-5">
              <a href="https://source.unsplash.com/random/247x247" data-toggle="lightbox" data-gallery="img-gallery" style="height: 35rem; width: 25rem;" >
              <img src="https://source.unsplash.com/random/247x247" alt="" class="img-fluid">
              </a>
            </div>
          
            <div class="col-mb-4 mx-auto pt-5 ml-5">
              <a href="https://source.unsplash.com/random/248x248" data-toggle="lightbox" data-gallery="img-gallery" style="height: 35rem; width: 25rem;" >
              <img src="https://source.unsplash.com/random/248x248" alt="" class="img-fluid">
              </a>
            </div>
          
            <div class="col-mb-4 mx-auto pt-5 ml-5">
              <a href="https://source.unsplash.com/random/249x249" data-toggle="lightbox" data-gallery="img-gallery" style="height: 35rem; width: 25rem;">
              <img src="https://source.unsplash.com/random/249x249" alt="" class="img-fluid">
              </a>
            </div>
          </div>
        </div>
     </section>
    </div>
  </div>

 <!-- Season gallery-->
  <h1 class="display-4 text-center m-y-3 text-muted" id="speakers">Season Trend</h1>
<div class="container">
  <div class="row">
    <div class="col-md-3 col-lg-2">
      <div class="card">
        <img alt="Card image cap" class="card-img-top img-fluid" src="https://source.unsplash.com/random/250x250" style="width: 10rem; height: 10rem;" />
        <div class="card-block">
          <h4 class="card-title">Lorem</h4>
        </div>
      </div>
    </div>
  
    <div class="col-md-3 col-lg-2">
      <div class="card">
        <img alt="Card image cap" class="card-img-top img-fluid" src="https://source.unsplash.com/random/251x251" style="width: 10rem; height: 10rem;" />
        <div class="card-block">
          <h4 class="card-title">Lorem</h4>
        </div>
      </div>
    </div>
  
    <div class="col-md-3 col-lg-2">
      <div class="card">
        <img alt="Card image cap" class="card-img-top img-fluid" src="https://source.unsplash.com/random/252x252" style="width: 10rem; height: 10rem;" />
        <div class="card-block">
          <h4 class="card-title">Lorem</h4>
        </div>
      </div>
    </div>
  
    <div class="col-md-3 col-lg-2">
      <div class="card">
        <img alt="Card image cap" class="card-img-top img-fluid" src="https://source.unsplash.com/random/253x253" style="width: 10rem; height: 10rem;" />
        <div class="card-block">
          <h4 class="card-title">Lorem</h4>
        </div>
      </div>
    </div>
  
    <div class="col-md-3 col-lg-2">
      <div class="card">
        <img alt="Card image cap" class="card-img-top img-fluid" src="https://source.unsplash.com/random/254x254" style="width: 10rem; height: 10rem;" />
        <div class="card-block">
          <h4 class="card-title">Lorem</h4>
        </div>
      </div>
    </div>
  
    <div class="col-md-3 col-lg-2">
      <div class="card">
        <img alt="Card image cap" class="card-img-top img-fluid" src="https://source.unsplash.com/random/255x255" style="width: 10rem; height: 10rem;" />
        <div class="card-block">
          <h4 class="card-title">Lorem</h4>
        </div>
      </div>
    </div>
  </div>
</div>

  <!--Signup-->
  <section class="text-center p-5 bg-dark text-white">
    <div class="container">
      <div class="row">
        <div class="col">
          <h1>Sign Up for Offers</h1>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis
             impedit dolores praesentium laborum veniam officia dolore nobis 
             beatae nihil libero!
          </p>
          <form class="form-inline justify-content-center">
            <input type="text" class="form-control mb-2 mr-2" placeholder="Enter Name">
            <input type="text" class="form-control mb-2 mr-2" placeholder="Enter Email">
            <button class="btn btn-primary mb-2">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </section>

  <!--Footer-->
  <footer id="main-footer" class="text-center p-4">
     <div class="container">
       <div class="row">
         <div class="col">
           <p>Copyright &copy;<span id="year">Fashion Ecommerce</span></p>
         </div>
       </div>
     </div>
  </footer>







  
   
  <script src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous"></script>   
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" 
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" 
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" 
    integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" 
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js" 
    integrity="sha256-Y1rRlwTzT5K5hhCBfAFWABD4cU13QGuRN6P5apfWzVs="
     crossorigin="anonymous"></script>

   <script>
     //Get the current year for the copyright
     $('#year').text(new Data().getFullYear());
     //Init 
       $('[data-toggle="popover"]').popover();
     
     //carousel
     $('.carousel').carousel({
       interval:6000,
       pause:'hover'
     })
   </script> 
</body>
</html>