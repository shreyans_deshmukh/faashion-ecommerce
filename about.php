<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-quiv="X-UA-Compatibe" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css" integrity="sha384-Bfad6CLCknfcloXFOyFnlgtENryhrpZCe29RTifKEixXQZ38WheV+i/6YWSzkz3V" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css" integrity="sha512-wR4oNhLBHf7smjy0K4oqzdWumd+r5/+6QO/vDda76MW5iug4PT7v86FoEkySIJft3XA0Ae6axhIvHrqwm793Nw==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css" integrity="sha512-6lLUdeQ5uheMFbWm3CP271l14RsX1xtx+J5x2yeIDkkiBpeVTNhTqijME7GgRKKi6hCqovwCoBTlRBEC20M8Mg==" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css">
    <title>Fashion Ecommerce</title>
</head>


<body data-spy="scroll" data-target="#main-nav" >
  
  
  <!--logo-bar-->
  <!--navbar-->
  <?php require('include/navbar.php');?>
  
  <!--Page Header-->
  <header id="page-header">
      <div class="container">
          <div class="row">
              <div class="col-md-6 m-auto text-center">
                  <h2>About Us</h2>
                  <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab, laborum!</p>
              </div>
          </div>
      </div>
  </header>

  <!--About section-->
  <section id="about" class="py-3">
      <div class="container">
          <div class="row">
              <div class="col-md-6 ">
                  <h1>What We Do</h1>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis perferendis
                     quos blanditiis repudiandae aliquid eaque, doloremque aliquam id exercitationem
                     rem autem quam magni qui minus sunt, suscipit quis, tempore culpa? Vero quo
                     officia officiis exercitationem, totam nobis deleniti reprehenderit ut quas 
                     tempora repellendus nesciunt earum sunt repellat fuga iusto repudiandae.</p>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis perferendis
                     quos blanditiis repudiandae aliquid eaque, doloremque aliquam id exercitationem
                     rem autem quam magni qui minus sunt, suscipit quis, tempore culpa? Vero quo
                     officia officiis exercitationem, totam nobis deleniti reprehenderit ut quas 
                     tempora repellendus nesciunt earum sunt repellat fuga iusto repudiandae.</p>
              </div>
              <div class="col-md-6">
                  <img src="img/702620.png" alt=""
                  class="img-fluid rounded-circle d-none d-md-block about-img">
              </div>
          </div>
      </div>
  </section>
  <section id="about" class="py-3">
    <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src="img/b2b_img1.jpg" alt=""
            class="img-fluid rounded-circle d-none d-md-block about-img">
        </div>
            <div class="col-md-6 ">
                <h1>What We Want To Do</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis perferendis
                   quos blanditiis repudiandae aliquid eaque, doloremque aliquam id exercitationem
                   rem autem quam magni qui minus sunt, suscipit quis, tempore culpa? Vero quo
                   officia officiis exercitationem, totam nobis deleniti reprehenderit ut quas 
                   tempora repellendus nesciunt earum sunt repellat fuga iusto repudiandae.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis perferendis
                   quos blanditiis repudiandae aliquid eaque, doloremque aliquam id exercitationem
                   rem autem quam magni qui minus sunt, suscipit quis, tempore culpa? Vero quo
                   officia officiis exercitationem, totam nobis deleniti reprehenderit ut quas 
                   tempora repellendus nesciunt earum sunt repellat fuga iusto repudiandae.</p>
            </div>
        </div>
    </div>
</section>


<!--Testimonials-->
  <section id="testimonials" class="p-4 bg-dark text-white">
    <div class="container">
      <h2 class="text-center">Testimonials</h2>
      <div class="row text-center">
        <div class="col">
          <div class="slider">
            <div>
              <blockquote class="blockquote">
                <p class="mb-0">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque, corrupti.
                </p>
                <footer class="blockquote-footer">Lorem, ipsum dolor.
                  <cite title="Company 1">Company 1</cite>
                </footer>
              </blockquote>
            </div>
            <div>
              <blockquote class="blockquote">
                <p class="mb-0">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque, corrupti.
                </p>
                <footer class="blockquote-footer">Lorem, ipsum dolor.
                  <cite title="Company 1">Company 1</cite>
                </footer>
              </blockquote>
            </div>
            <div>
              <blockquote class="blockquote">
                <p class="mb-0">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque, corrupti.
                </p>
                <footer class="blockquote-footer">Lorem, ipsum dolor.
                  <cite title="Company 1">Company 1</cite>
                </footer>
              </blockquote>
            </div>
          </div>
        </div>
      </div>   
    </div>
  </section>


  <!--Footer-->
  <footer id="main-footer" class="text-center p-4">
    <div class="container">
      <div class="row">
        <div class="col">
          <p>Copyright &copy;<span id="year">Fashion Ecommerce</span></p>
        </div>
      </div>
    </div>
 </footer>







  

  <script src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous"></script>   
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" 
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" 
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" 
    integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" 
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.js" 
    integrity="sha512-eP8DK17a+MOcKHXC5Yrqzd8WI5WKh6F1TIk5QZ/8Lbv+8ssblcz7oGC8ZmQ/ZSAPa7ZmsCU4e/hcovqR8jfJqA==" 
    crossorigin="anonymous"></script>
    

   <script>
     //Get the current year for the copyright
     $('#year').text(new Date().getFullYear());

     $('.slider').slick({
       infinite: true,
       slideToShow: 1,
       slideToScroll: 1
     });
     
   </script> 
</body>
</html>