<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-quiv="X-UA-Compatibe" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css" integrity="sha384-Bfad6CLCknfcloXFOyFnlgtENryhrpZCe29RTifKEixXQZ38WheV+i/6YWSzkz3V" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Fashion Ecommerce</title>
</head>


<body data-spy="scroll" data-target="#main-nav" >
  
  
  <!--logo-bar-->
  <!--navbar-->
  <?php require('include/navbar.php');?>
  
  <!--Page Header-->
  <header id="page-header">
      <div class="container">
          <div class="row">
              <div class="col-md-6 m-auto text-center">
                  <h2>Read Our Blog</h2>
                  <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ab, laborum!</p>
              </div>
          </div>
      </div>
  </header>

  <!--Blog-->
  <section id="blog" class="py-3">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="card-columns">
            <div class="card">
              <img src="https://source.unsplash.com/random/300x200" alt="" class="img-fluid card-img-top">
              <div class="card-body">
                <h4 class="card-title">Blog Post One</h4>
                <small class="text-muted">Written By Charles on 10/20</small>
                <hr>
                <p class="card-text">Lorem ipsum dolor sit amet consectetur, 
                  adipisicing elit. Hic vel, at corrupti quisquam asperiores veritatis.</p>
              </div>
            </div>
             <div class="card p-3 bg-danger text-white">
               <blockquote class="card-blockquote card-body">
                 <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                    Corrupti animi quos modi veniam, beatae architecto sunt quasi aperiam in. Mollitia.
                  </p>
                  <footer class="blockquote-footer">
                    <small class="text-white">Lorem, ipsum dolor.<cite title="Source Title"></cite>Source title</small>
                  </footer>
               </blockquote>
             </div>
             <div class="card">
              <img src="https://source.unsplash.com/random/301x201" alt="" class="img-fluid card-img-top">
              <div class="card-body">
                <h4 class="card-title">Blog Post Two</h4>
                <small class="text-muted">Written By David on 1/20</small>
                <hr>
                <p class="card-text">Lorem ipsum dolor sit amet consectetur, 
                  adipisicing elit. Hic vel, at corrupti quisquam asperiores veritatis.</p>
              </div>
            </div>
             <div class="card p-3 bg-primary text-white">
               <blockquote class="card-blockquote card-body">
                 <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                    Corrupti animi quos modi veniam, beatae architecto sunt quasi aperiam in. Mollitia.
                  </p>
                  <footer class="blockquote-footer">
                    <small class="text-white">Lorem, ipsum dolor.<cite title="Source Title"></cite>Source title</small>
                  </footer>
               </blockquote>
             </div>
             <div class="card">
              <img src="https://source.unsplash.com/random/302x202" alt="" class="img-fluid card-img-top">
              <div class="card-body">
                <h4 class="card-title">Blog Post Three</h4>
                <small class="text-muted">Written By John on 12/2</small>
                <hr>
                <p class="card-text">Lorem ipsum dolor sit amet consectetur, 
                  adipisicing elit. Hic vel, at corrupti quisquam asperiores veritatis.</p>
              </div>
            </div>
             <div class="card p-3 bg-danger text-white">
               <blockquote class="card-blockquote card-body">
                 <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                    Corrupti animi quos modi veniam, beatae architecto sunt quasi aperiam in. Mollitia.
                  </p>
                  <footer class="blockquote-footer">
                    <small class="text-white">Lorem, ipsum dolor.<cite title="Source Title"></cite>Source title</small>
                  </footer>
               </blockquote>
             </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!--Footer-->
  <footer id="main-footer" class="text-center p-4">
    <div class="container">
      <div class="row">
        <div class="col">
          <p>Copyright &copy;<span id="year">Fashion Ecommerce</span></p>
        </div>
      </div>
    </div>
 </footer>







  

  <script src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous"></script>   
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" 
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" 
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" 
    integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" 
    crossorigin="anonymous"></script>
  
    

   <script>
     //Get the current year for the copyright
     $('#year').text(new Date().getFullYear());
   </script> 
</body>
</html>